.PHONY: main debug

flags = -Wall -Werror -pedantic -std=c++14
optimise = -O3
debug = -ggdb
files = lib/{main,lex,parse,grammar}.cpp
ipath = -I include/

main:
	g++ $(flags) $(optimise) $(files) $(ipath) -o main

debug:
	g++ $(flags) $(debug)    $(files) $(ipath) -o debug/main
