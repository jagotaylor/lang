#ifndef LEX_INCLUDED
#define LEX_INCLUDED

#include <utility>
#include <string>
#include <vector>
#include <deque>
#include <regex>

namespace lex {

typedef std::string String;
typedef std::vector<std::pair<String,String>> StringPairs;
typedef std::vector<std::pair<std::regex,String>> RegexPairs;

struct Token {
  String type;
  String data;
  Token(String, String);
  bool operator==(const Token&) const;
};

static Token null("", "");

class Iterator;

class Lex {
  RegexPairs regexPairs;
  String string;
  String::const_iterator pos, pend;
  std::vector<String> skippable;
  std::deque<Token> tokens;
  bool parseNext();
public:
  Lex(String, String, std::vector<String> ={});
  Lex(StringPairs, String, std::vector<String> ={});
  Lex(const Lex&);
  Iterator begin();
  Iterator end();
  Token& operator[] (int);
  StringPairs static parsePairs(std::string);
};

class Iterator {
  int index;
  Lex* parent;
public:
  Iterator(int, Lex*);
  Iterator(const Iterator&); // copy constructor
  Iterator& operator++(); // ++i
  Iterator& operator--(); // --i
  Iterator operator++(int); // i++
  Iterator operator--(int); // i--
  Token& operator*();
  Token* operator->();
  bool operator==(const Iterator&) const;
  bool operator!=(const Iterator&) const;
};

}

#endif // LEX_INCLUDED
