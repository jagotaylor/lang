#include <list>
#include <iostream>

template<class T>
struct Stack {
  std::list<T> list;
  void push(T);
  T pop();
  T peek();
  bool isEmpty();
};

template<class T>
void Stack<T>::push(T t) {
  list.push_back(t);
}

template<class T>
T Stack<T>::pop() {
  if (list.empty()) {
    std::cerr << "Error: Nothing to pop from stack" << std::endl;
    exit(1);
  }
  auto temp = list.back();
  list.pop_back();
  return temp;
}

template<class T>
T Stack<T>::peek() {
  return list.back();
}

template<class T>
bool Stack<T>::isEmpty() {
  return list.empty();
}
