#ifndef PARSE_INCLUDED
#define PARSE_INCLUDED

#include <map>
#include <list>
#include <string>
#include <memory>

#include "lex.hpp"

namespace parse {

struct Expression;
struct Tree;
struct NonTerminal;
struct Terminal;
typedef std::unique_ptr<Tree> TreeP;


class Grammar {
  std::map<std::string,Expression*> rules;
public:
  Grammar(std::string);
  Expression* operator[] (std::string);
  TreeP operator() (lex::Iterator, lex::Iterator);
};


struct Tree {
  virtual ~Tree() = default;
  virtual Tree* absorb(Tree* t) = 0;
  virtual Tree* joins(NonTerminal*) = 0;
  virtual Tree* joins(Terminal*) = 0;
  virtual void print() = 0;
};

struct NonTerminal : public Tree {
  Tree* absorb(Tree*) override;
  Tree* joins(NonTerminal*) override;
  Tree* joins(Terminal*) override;
  void print() override;
  std::string name;
  std::list<Tree*> children;
};

struct Terminal : public Tree {
  Tree* absorb(Tree*) override;
  Tree* joins(NonTerminal*) override;
  Tree* joins(Terminal*) override;
  void print() override;
  lex::Token token;
  Terminal(lex::Token t) : token(t) {};
};


struct Expression {
  virtual ~Expression() = default;
  virtual TreeP parse(lex::Iterator&, const lex::Iterator&) = 0;
};

struct Concatenation : public Expression {
  Expression *l, *r;
  virtual TreeP parse(lex::Iterator&, const lex::Iterator&);
};

struct Disjunction : public Expression {
  Expression *l, *r;
  virtual TreeP parse(lex::Iterator&, const lex::Iterator&);
};

// struct Repetition : public Expression {
//   Expression *n;
//   virtual TreeP parse(lex::Iterator&, const lex::Iterator&);
// };

struct Rule : public Expression {
  std::string rule;
  Grammar *parent;
  virtual TreeP parse(lex::Iterator&, const lex::Iterator&);
};

struct Token : public Expression {
  std::string type;
  virtual TreeP parse(lex::Iterator&, const lex::Iterator&);
};

struct String : public Expression {
  std::string string;
  virtual TreeP parse(lex::Iterator&, const lex::Iterator&);
};

}

#endif // PARSE_INCLUDED
