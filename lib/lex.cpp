#include "lex.hpp"
#include <algorithm>

using namespace lex;

Token::Token(String t, String s) : type(t), data(s) {}

bool Token::operator==(const Token &t) const {
  return (type == t.type) && (data == t.data);
}

Lex::Lex(String r, String s, std::vector<String> skip) : skippable(skip) {
  string = s;
  pos = string.begin();
  pend = string.end();
  for (auto& p : parsePairs(r)) {
    regexPairs.emplace_back(std::regex(p.first), p.second);
  }
}

Lex::Lex(StringPairs ps, String s, std::vector<String> skip) : skippable(skip) {
  string = s;
  pos = string.begin();
  pend = string.end();
  for (auto& p : ps) {
    regexPairs.emplace_back(std::regex(p.first), p.second);
  }
}

Lex::Lex(const Lex& l) : regexPairs(l.regexPairs), pos(l.pos), pend(l.pend) {}

Iterator Lex::begin() {
  Iterator i(0, this);
  ++i;
  --i;
  return i;
}

Iterator Lex::end() {
  return Iterator(-1, this);
}

Token& Lex::operator[] (int index) {
  while (index >= tokens.size()) {
    if (!parseNext()) {
      return null;
    }
  }
  return tokens[index];
}

bool Lex::parseNext() {
  std::smatch match;
  for (auto pair : regexPairs) {
    if (std::regex_search(pos, pend, match, pair.first, std::regex_constants::match_continuous)) {
      pos = match.suffix().first;
      if (std::find(skippable.begin(), skippable.end(), pair.second) == skippable.end()) {
        tokens.emplace_back(pair.second, match.str());
      }
      return true;
    }
  }
  return false;
}

lex::StringPairs Lex::parsePairs(std::string s) {
  lex::StringPairs pairs;
  std::regex regex("([A-Z]+)\\s*:(.+)\n?");
  auto pos = s.cbegin();
  auto end = s.cend();
  std::smatch match;
  auto flags = std::regex_constants::match_continuous;
  while (pos != end) {
    if (std::regex_search(pos, end, match, regex, flags)) {
      pairs.emplace_back(match[2], match[1]);
      pos = match.suffix().first;
    } else {
      ++pos;
    }
  }
  return pairs;
}

Iterator::Iterator(int i, Lex* l) : index(i), parent(l) {}

Iterator::Iterator(const Iterator& i) : index(i.index), parent(i.parent) {}


Iterator& Iterator::operator++() {
  if (index > -1) {
    index++;
    if ((*parent)[index] == null) {
      index = -1;
    }
  }
  return (*this);
}

Iterator& Iterator::operator--() {
  if (index > 0) {
    index--;
  }
  return (*this);
}

Iterator Iterator::operator++(int) {
  auto temp = *this;
  operator++();
  return temp;
}

Iterator Iterator::operator--(int) {
  auto temp = *this;
  operator--();
  return temp;
}

Token& Iterator::operator*() {
  return (*parent)[index];
}

Token* Iterator::operator->() {
  return &operator*();
}

bool Iterator::operator==(const Iterator &i) const {
  return (index == i.index) && (parent == i.parent);
}

bool Iterator::operator!=(const Iterator &i) const {
  return !operator==(i);
}
