#include <stack.hpp>

typedef Stack<lex::Token> tokenStack;
typedef Stack<Expression*> expressionStack;
void print(const tokenStack &s);
void print(const expressionStack &s);

class Shifter {
  void addOperator(lex::Token);
  void addExpression(lex::Token);
  void popBracket();
  void flush();
  tokenStack operators;
  expressionStack expressions;
  Grammar* parent;
  lex::Iterator token, end;
public:
  std::map<std::string,Expression*> rules;
  Shifter(Grammar*, lex::Iterator, lex::Iterator);
  operator std::map<std::string,Expression*>() { return rules; };
};

int prec(lex::Token t) {
  auto s = t.data;
  if (s == "->") return 1;
  if (s == "|")  return 2;
  if (s == "&")  return 3;
  return 4;
}

Shifter::Shifter(Grammar* p, lex::Iterator a, lex::Iterator z) : parent(p), token(a), end(z) {
  while (token != end) {
    if (token->type == "OPERATOR" || token->data == "(") {
      addOperator(*token);
    } else {
      if (token->data == ")") {
        addOperator(*token);
      } else {
        addExpression(*token);
      }
      ++token;
      if (token != end) {
        if (token->type != "OPERATOR" && token->data != ")") {
          addOperator(lex::Token("OPERATOR", "&"));
        }
      }
      --token;
    }
    ++token;
  }
  flush();
}

void Shifter::addOperator(lex::Token t) {
  if (operators.isEmpty()) {
    operators.push(t);
  } else
  if (t.data == ")") {
    popBracket();
  } else
  if (operators.peek().data == "(") {
    operators.push(t);
  } else
  if (prec(operators.peek()) > prec(t)) {
    addExpression(operators.pop());
    addOperator(t);
  } else {
    operators.push(t);
  }
}

void Shifter::addExpression(lex::Token token) {
  if (token.data == "|") {
    auto d = new Disjunction();
    d->r = expressions.pop();
    d->l = expressions.pop();
    expressions.push(d);
  } else
  if (token.data == "&") {
    auto c = new Concatenation();
    c->r = expressions.pop();
    c->l = expressions.pop();
    expressions.push(c);
  } else
  if (token.data == "->") {
    auto e = expressions.pop();
    Rule* r = dynamic_cast<Rule*>(expressions.pop());
    rules.emplace(r->rule, e);
  } else
  if (token.type == "RULE") {
    auto r = new Rule();
    r->rule = token.data;
    r->parent = parent;
    expressions.push(r);
  } else
  if (token.type == "TOKEN") {
    auto t = new Token();
    t->type = token.data;
    expressions.push(t);
  } else
  if (token.type == "STRING") {
    auto s = new String();
    auto len = token.data.length();
    s->string = token.data.substr(1, len-2);
    expressions.push(s);
  }
}

void Shifter::popBracket() {
  while (!operators.isEmpty()) {
    auto t = operators.pop();
    if (t.data == "(") return;
    addExpression(t);
  }
}

void Shifter::flush() {
  while (!operators.isEmpty()) {
    addExpression(operators.pop());
  }
}
