#include "parse.hpp"
#include <iostream>

using namespace parse;

TreeP Concatenation::parse(lex::Iterator& i, const lex::Iterator& e) {
  if (i == e) {
    return TreeP(nullptr);
  }
  auto j = i;
  auto a = l->parse(j, e);
  auto b = r->parse(j, e);
  if (a && b ) {
    i = j;
    auto ab = TreeP(a->absorb(b.get()));
    a.release();
    b.release();
    return ab;
  }
  return TreeP(nullptr);
}

TreeP Disjunction::parse(lex::Iterator& i, const lex::Iterator& e) {
  if (i == e) {
    return TreeP(nullptr);
  }
  auto j = i;
  auto a = l->parse(j, e);
  if (a) {
    i = j;
    return a;
  }
  auto b = r->parse(j, e);
  if (b) {
    i = j;
    return b;
  }
  return TreeP(nullptr);
}

// TreeP Repetition::parse(lex::Iterator& i, const lex::Iterator& e) {
//   if (i == e) {
//     return TreeP(nullptr);
//   }
//   return TreeP(nullptr);
// }

TreeP Rule::parse(lex::Iterator& i, const lex::Iterator& e) {
  if (i == e) {
    return TreeP(nullptr);
  }
  auto r = (*parent)[rule]->parse(i, e);
  if (r) {
    auto nt = new NonTerminal();
    nt->children = {r.get()};
    r.release();
    return TreeP(nt);
  }
  return r;
}

TreeP Token::parse(lex::Iterator& i, const lex::Iterator& e) {
  if (i == e) {
    return TreeP(nullptr);
  }
  if (i->type == type) {
    return TreeP(new Terminal(*(i++)));
  }
  return TreeP(nullptr);
}

TreeP String::parse(lex::Iterator& i, const lex::Iterator& e) {
  if (i == e) {
    return TreeP(nullptr);
  }
  if (i->data == string) {
    return TreeP(new Terminal(*(i++)));
  }
  return TreeP(nullptr);
}

Tree* NonTerminal::absorb(Tree* t) {
  return t->joins(this);
}

Tree* NonTerminal::joins(NonTerminal* t) {
  auto& c = t->children;
  c.splice(c.end(), children);
  delete this;
  return t;
}

Tree* NonTerminal::joins(Terminal* t) {
  children.push_front(t);
  return this;
}


Tree* Terminal::absorb(Tree* t) {
  return t->joins(this);
}

Tree* Terminal::joins(NonTerminal* t) {
  t->children.push_back(this);
  return t;
}

Tree* Terminal::joins(Terminal* t) {
  auto n = new NonTerminal();
  n->children = {t, this};
  return n;
}

void NonTerminal::print() {
  std::cout << name << "{";
  for (auto& t : children) {
    t->print();
    std::cout << " ";
  }
  std::cout << "}";
}

void Terminal::print() {
  std::cout << token.data;
}
