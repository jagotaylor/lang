#include <iostream>
#include "lex.hpp"
#include "parse.hpp"

std::string regex = "INT :[0-9]+\nOP :\\+|-\nWS :\\s+";
std::string grammar = "exp -> INT (\"+\"|\"-\") exp | INT";

int main() {
  std::string input;
  std::cout << ">> ";
  std::getline(std::cin, input);

  lex::Lex lexer(regex, input, {"WS"});
  parse::Grammar parser(grammar);
  auto result = parser(lexer.begin(), lexer.end());

  std::cout << ">> ";
  result->print();
  std::cout << std::endl;

  return 0;
}
