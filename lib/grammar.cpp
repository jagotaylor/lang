#include "parse.hpp"

std::string tokenRegexes = R"--(
RULE       :[a-z][a-zA-Z]*
TOKEN      :[A-Z]+
STRING     :".*?"
OPERATOR   :\||->
WHITESPACE :\s+
BRACKET    :[()]
)--";

using namespace parse;

#include "shifter.cpp"

Grammar::Grammar(std::string input) {
  lex::Lex lexer(tokenRegexes, input, {"WHITESPACE"});
  rules = Shifter(this, lexer.begin(), lexer.end()).rules;
}

Expression* Grammar::operator[] (std::string s) {
  return rules.at(s);
}

TreeP Grammar::operator() (lex::Iterator i, lex::Iterator e) {
  auto firstIt = rules.begin();
  if (firstIt == rules.end()) {
    return TreeP(nullptr);
  }
  // First Expression is second element in pair
  auto first = firstIt->second;
  auto result = first->parse(i, e);
  return result;
}
